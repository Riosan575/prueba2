﻿using Examen_T1.DB;
using Examen_T1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T1.Controllers
{
    public class ComentDetalle
    {
        public Post post { get; set; }
        public List<Comentario> Comentarios { get; set; }
    }
    public class HomeController : Controller
    {
        private BlogContext _context;
        public HomeController(BlogContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            var blog = _context.posts;
            var ordenar = _context.posts.OrderByDescending(o => o.Fecha);
            return View(ordenar);
        }
        public IActionResult Registrar()
        {
            return View();
        }       
        [HttpPost]
        public ActionResult Registrar(Post post)
        {
            post.Fecha = DateTime.Now;
            _context.posts.Add(post);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ViewResult Detalle(int id)
        {
            var post = _context.posts;
            Post pos = post.FirstOrDefault(item => item.Id == id);
            List<Comentario> comentarios = _context.Comentarios.Where(o => o.Postid == id).
                OrderByDescending(o => o.Fecha).ToList();

            var detalle = new ComentDetalle
               {
                   post = pos,
                   Comentarios = comentarios
               };
            
            return View(detalle);
        }
        [HttpPost]
        public RedirectToActionResult Detalle(Comentario comentario)
        {
            comentario.Fecha = DateTime.Now;
            _context.Comentarios.Add(comentario);
            _context.SaveChanges();
            return RedirectToAction("Detalle", new { id = comentario.Postid});
        }
    }
}
